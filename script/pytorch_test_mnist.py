from __future__ import print_function
import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.optim.lr_scheduler import StepLR
from queue import Queue
import copy
import random
import matplotlib.pyplot as plt
import numpy as np

from torchsummary import summary
import os


# from pymemcache.client import base


class itemData(object):
    def __init__(self, my_input, my_label):
        self.input = my_input
        self.label = my_label

    def get_input(self):
        return self.input

    def get_label(self):
        return self.label


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 32, 3, 1)
        self.conv2 = nn.Conv2d(32, 64, 3, 1)
        self.dropout1 = nn.Dropout(0.25)
        self.dropout2 = nn.Dropout(0.5)
        self.fc1 = nn.Linear(9216, 128)
        self.fc2 = nn.Linear(128, 10)

    def forward(self, x):
        x = self.conv1(x)
        x = F.relu(x)
        x = self.conv2(x)
        x = F.relu(x)
        x = F.max_pool2d(x, 2)
        x = self.dropout1(x)
        x = torch.flatten(x, 1)
        x = self.fc1(x)
        x = F.relu(x)
        x = self.dropout2(x)
        x = self.fc2(x)
        output = F.log_softmax(x, dim=1)
        return output

##### New configuration ######
q_data = []
hashLabel = {}
hashLabel[0] = 0
hashLabel[1] = 0 
hashLabel[2] = 0 
hashLabel[3] = 0
hashLabel[4] = 0
hashLabel[5] = 0
hashLabel[6] = 0
hashLabel[7] = 0
hashLabel[8] = 0
hashLabel[9] = 0

# Don't forget to run `memcached' before running this next line
# client = base.Client(('localhost', 11211))


def ravel_model_params(model, grads=False):
    """
    Squash model parameters or gradients into a single tensor.
    """
    # torch.set_default_tensor_type(torch.cuda.FloatTensor)
    m_parameter = torch.Tensor([0]) #### cuda
    # device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    device = torch.device("cpu")
    m_parameter = m_parameter.to(device)

    for parameter in list(model.parameters()):
        if grads:
            # m_parameter = torch.cat((m_parameter, parameter.grad.view(-1)))
            temp = parameter.grad.view(-1)
            # device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
            temp = temp.to(device)
            # m_parameter = torch.cat((m_parameter, parameter.grad.view(-1)))
            m_parameter = torch.cat((m_parameter, temp))
        else:
            temp2 = parameter.data.view(-1)
            temp2 = temp2.to(device)
            # m_parameter = torch.cat((m_parameter, parameter.data.view(-1)))
            m_parameter = torch.cat((m_parameter, temp2))
    return m_parameter[1:]



def train(args, model, device, train_loader, optimizer, epoch):
    global q_data
    global hashLabel
    global client

    max_iteration = (epoch+1) * 200 + 100

    local_iter = 0

    # model.train()
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.to(device), target.to(device)

        item = itemData(my_input=copy.deepcopy(data), my_label=copy.deepcopy(target))

        max_len_q_data = (10000) ### biased

        if len(q_data) > max_len_q_data:
            del q_data[0]
        else:
            q_data.append(item)

        # Randome data.
        # Generates a random number between 
        # a given positive range 
        r1 = random.randint(0, len(q_data)-1)
        # randomize the data.

        i_counter = 0
        copy_data = q_data[r1]

        # Set data
        inputs = copy_data.get_input()
        labels = copy_data.get_label()

        # print(labels)

        # print(labels.item())

        if len(labels) > 0:
            labels_1 = labels[0]
            hashLabel[labels_1.item()] = hashLabel[labels_1.item()] + 1

        optimizer.zero_grad()
        
        output = model(data)
        
        loss = F.nll_loss(output, target)

        loss.backward()

        optimizer.step()

        ##### New configuration #####
        # temp_weight = ravel_model_params(model)
        # accumulated_gradients = torch.zeros(ravel_model_params(model).size())

        # model_size = len(accumulated_gradients)
        # print("model_size={}".format(model_size))
        # string_buffer = "11111111111111111"
        # for l in range(model_size):
        #     string_buffer = string_buffer + "0"

        # client.set('some_key', string_buffer)

        # # test
        # test_accumulated_gradients = client.get('some_key')
        # model_size2 = len(test_accumulated_gradients)
        # print("test_weight={}".format(model_size2))

        # assert(model_size == model_size2)


        if batch_idx % args.log_interval == 0:
            # print(hashLabel)
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.item()))
            if args.dry_run:
                break

        # local_iter = local_iter + 1
        # if local_iter > max_iteration:
        #     break


def test(model, device, test_loader):
    model.eval()
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in test_loader:
            data, target = data.to(device), target.to(device)
            output = model(data)
            test_loss += F.nll_loss(output, target, reduction='sum').item()  # sum up batch loss
            pred = output.argmax(dim=1, keepdim=True)  # get the index of the max log-probability
            correct += pred.eq(target.view_as(pred)).sum().item()

    test_loss /= len(test_loader.dataset)

    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        test_loss, correct, len(test_loader.dataset),
        100. * correct / len(test_loader.dataset)))


def main():
    # global hashLabel

    # Training settings
    parser = argparse.ArgumentParser(description='PyTorch MNIST Example')
    parser.add_argument('--batch-size', type=int, default=128, metavar='N',
                        help='input batch size for training (default: 64)')
    parser.add_argument('--test-batch-size', type=int, default=1000, metavar='N',
                        help='input batch size for testing (default: 1000)')
    parser.add_argument('--epochs', type=int, default=1, metavar='N',
                        help='number of epochs to train (default: 14)')
    parser.add_argument('--lr', type=float, default=1.0, metavar='LR',
                        help='learning rate (default: 1.0)')
    parser.add_argument('--gamma', type=float, default=0.7, metavar='M',
                        help='Learning rate step gamma (default: 0.7)')
    parser.add_argument('--no-cuda', action='store_true', default=False,
                        help='disables CUDA training')
    parser.add_argument('--dry-run', action='store_true', default=False,
                        help='quickly check a single pass')
    parser.add_argument('--seed', type=int, default=1, metavar='S',
                        help='random seed (default: 1)')
    parser.add_argument('--log-interval', type=int, default=100, metavar='N',
                        help='how many batches to wait before logging training status')
    parser.add_argument('--save-model', action='store_true', default=False,
                        help='For Saving the current Model')
    args = parser.parse_args()
    use_cuda = not args.no_cuda and torch.cuda.is_available()

    torch.manual_seed(args.seed)

    device = torch.device("cuda" if use_cuda else "cpu")

    train_kwargs = {'batch_size': args.batch_size}
    test_kwargs = {'batch_size': args.test_batch_size}
    if use_cuda:
        cuda_kwargs = {'num_workers': 1,
                       'pin_memory': True,
                       'shuffle': True}
        train_kwargs.update(cuda_kwargs)
        test_kwargs.update(cuda_kwargs)

    transform=transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.1307,), (0.3081,))
        ])

    # MNIST
    # dataset1 = datasets.MNIST('../data', train=True, download=True,
    #                    transform=transform)

    # dataset2 = datasets.MNIST('../data', train=False,
    #                    transform=transform)


    # FashionMNIST
    # dataset1 = datasets.FashionMNIST('../data', train=True, download=True,
    #                    transform=transform)

    # dataset2 = datasets.FashionMNIST('../data', train=False,
    #                    transform=transform)

    # KMNIST
    # dataset1 = datasets.KMNIST('../data', train=True, download=True,
    #                    transform=transform)

    # dataset2 = datasets.KMNIST('../data', train=False,
    #                    transform=transform)


    # CIFAR-10
    dataset1 = datasets.CIFAR10('../data', train=True, download=True,
                       transform=transform)

    dataset2 = datasets.CIFAR10('../data', train=False,
                       transform=transform)

    train_loader = torch.utils.data.DataLoader(dataset1,**train_kwargs)
    test_loader = torch.utils.data.DataLoader(dataset2, **test_kwargs)

    os._exit(0)

    model = Net().to(device)



    ##### New configuration ######
    summary(model, (1, 28, 28))


    # model_size = len(model)
    # print("model_size={}".format(model_size))

    optimizer = optim.Adadelta(model.parameters(), lr=args.lr)

    scheduler = StepLR(optimizer, step_size=1, gamma=args.gamma)
    for epoch in range(1, args.epochs + 1):
        train(args, model, device, train_loader, optimizer, epoch)
        test(model, device, test_loader)
        scheduler.step()

    if args.save_model:
        torch.save(model.state_dict(), "mnist_cnn.pt")

    # plot the histogram...
    probability = []
    probability.append(hashLabel[0])
    probability.append(hashLabel[1])
    probability.append(hashLabel[2])
    probability.append(hashLabel[3])
    probability.append(hashLabel[4])
    probability.append(hashLabel[5])
    probability.append(hashLabel[6])
    probability.append(hashLabel[7])
    probability.append(hashLabel[8])
    probability.append(hashLabel[9])

    x = np.arange(len(probability))
    plt.bar(x, probability)
    plt.xticks(x, ['0','1','2','3','4','5','6','7','8','9'])
    plt.savefig("mnist_histogram.png")



if __name__ == '__main__':
    main()