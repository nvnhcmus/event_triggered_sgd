'''
This is the demo of power low
https://en.wikipedia.org/wiki/Power_law
'''

import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import time
import pandas as pd


TOTAL_SAMPLE = (60000//32)
NUM_CLIENTS = 10
sample_per_client = [0] * NUM_CLIENTS

for idx in range(NUM_CLIENTS):
    fraction_val = 1.0 / (1.0 + 0.5 * idx)
    num_iterations = int(TOTAL_SAMPLE * fraction_val)
    sample_per_client[idx] = num_iterations

print(sample_per_client)

fig, ax = plt.subplots()

geb_b30_x = np.arange(1, len(sample_per_client)+1)
bar_val = pd.Series(data=sample_per_client, index=geb_b30_x)
ax.set_xticks(geb_b30_x)

ax.plot(geb_b30_x, bar_val)

# ref: https://stackoverflow.com/questions/52240633/matplotlib-display-value-next-to-each-point-on-chart
# Matplotlib: Display value next to each point on chart
for i, v in enumerate(sample_per_client):
    ax.text(i+1, v+25, "%d" %v, ha="center")

# plot the figure
plt.ylabel('# of iteration')
plt.xlabel('Client')
# plt.legend(loc=4, ncol=1)

# set title
plt.title("Power law")

plt.grid(True)
# plt.tight_layout()

file_name = "power_law/iteration_power_law.png"
print("fileName={}".format(file_name))
fig.savefig(file_name)

plt.close(fig)    # close the figure window

# Done
os._exit(0)