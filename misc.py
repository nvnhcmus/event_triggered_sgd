import sys
import time

# miscellaneous

# Dataset
MNIST_DATASET = 0
FASHION_MNIST_DATASET = 1
CIFAR_10 = 2
CIFAR_100 = 3


# Type of dataset
IID_DATASET = 0
HETEROGENEOUS_DATASET = 1


# Flag
FLAG_ENABLE = (1)
FLAG_DISABLE = (0)


# some magic variables
FLAG_FINISHED_EPOCH = (99999999)